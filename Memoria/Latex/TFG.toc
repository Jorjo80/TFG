\select@language {spanish}
\contentsline {chapter}{Agradecimientos}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Resumen}{\es@scroman {vii}}{chapter*.2}
\contentsline {paragraph}{Palabras clave:}{\es@scroman {vii}}{section*.3}
\contentsline {chapter}{Abstract}{\es@scroman {ix}}{chapter*.4}
\contentsline {paragraph}{Keywords:}{\es@scroman {ix}}{section*.5}
\contentsline {chapter}{\numberline {1}Introducci\'on}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivaci\'on del proyecto}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Materiales utilizados}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Gesti\'on del proyecto}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Estructura del documento}{5}{section.1.5}
\contentsline {chapter}{\numberline {2}Estado del arte}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\'on a los Robots escaladores}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Definiciones}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Tipos de Robots escaladores}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Clasificaci\'on seg\'un tipo de superficies}{9}{subsection.2.3.1}
\contentsline {subsubsection}{Robots que caminan por suelos o paredes}{9}{section*.9}
\contentsline {subsubsection}{Robots que caminan por suelos y paredes}{10}{section*.10}
\contentsline {subsubsection}{Robots que caminan por vigas y columnas}{10}{section*.11}
\contentsline {subsection}{\numberline {2.3.2}Clasificaci\'on seg\'un los Dispositivo de Sujeci\'on}{11}{subsection.2.3.2}
\contentsline {subsubsection}{Ventosas de vac\IeC {\'\i }o}{11}{section*.12}
\contentsline {subsubsection}{Electroimanes}{12}{section*.13}
\contentsline {subsubsection}{Pinzas o abrazaderas}{12}{section*.14}
\contentsline {subsubsection}{Microespinas}{13}{section*.15}
\contentsline {subsection}{\numberline {2.3.3}Clasificaci\'on seg\'un tipo de Accionamiento}{14}{subsection.2.3.3}
\contentsline {subsubsection}{El\'ectrico}{14}{section*.16}
\contentsline {subsubsection}{Neum\'atico}{14}{section*.17}
\contentsline {subsubsection}{Mixto}{14}{section*.18}
\contentsline {section}{\numberline {2.4}Introducci\'on a los sistemas mecatr\'onicos}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}Sistemas Generadores de Vac\IeC {\'\i }o}{17}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Teor\IeC {\'\i }a de Vac\IeC {\'\i }o}{17}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Producci\'on de Vac\IeC {\'\i }o}{17}{subsection.2.5.2}
\contentsline {subsubsection}{Sensores de Vac\IeC {\'\i }o}{20}{section*.19}
\contentsline {section}{\numberline {2.6}Dispositivos Electromec\'anicos y El\'ectricos}{20}{section.2.6}
\contentsline {subsubsection}{Actuadores}{20}{section*.20}
\contentsline {subsection}{\numberline {2.6.1}Motor Brushless}{21}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Variador ESC}{21}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}Fotosensores}{22}{subsection.2.6.3}
\contentsline {subsection}{\numberline {2.6.4}Microcontroladores}{22}{subsection.2.6.4}
\contentsline {chapter}{\numberline {3}Optimizaci\'on del dise\~no de la turbina y el estator}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Estado de partida}{23}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Sistema de agarre}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Plataforma de pruebas}{26}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Pruebas experimentales}{29}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}An\'alisis del efecto del N\'umero de \'Alabes}{29}{subsection.3.2.1}
\contentsline {subsubsection}{Tablas de datos}{30}{section*.21}
\contentsline {subsubsection}{Gr\'aficas}{32}{section*.22}
\contentsline {subsection}{\numberline {3.2.2}An\'alisis del efecto del \'Angulo de los \'Alabes}{34}{subsection.3.2.2}
\contentsline {subsubsection}{Tablas de datos}{34}{section*.23}
\contentsline {subsubsection}{Gr\'aficas}{35}{section*.24}
\contentsline {subsection}{\numberline {3.2.3}An\'alisis del efecto del uso de \'Alabes Interiores}{37}{subsection.3.2.3}
\contentsline {subsubsection}{Tablas de datos}{37}{section*.25}
\contentsline {subsubsection}{Gr\'aficas}{38}{section*.26}
\contentsline {subsection}{\numberline {3.2.4}An\'alisis del efecto de la Altura del Rotor}{40}{subsection.3.2.4}
\contentsline {subsubsection}{Tablas de datos}{40}{section*.27}
\contentsline {subsubsection}{Gr\'aficas}{41}{section*.28}
\contentsline {subsection}{\numberline {3.2.5}An\'alisis del efecto del \'Angulo de las Toberas}{44}{subsection.3.2.5}
\contentsline {subsubsection}{Tablas de datos}{44}{section*.29}
\contentsline {subsubsection}{Gr\'aficas}{45}{section*.30}
\contentsline {subsection}{\numberline {3.2.6}An\'alisis del efecto del uso de \'Alabes Dobles}{47}{subsection.3.2.6}
\contentsline {subsubsection}{Tablas de datos}{47}{section*.31}
\contentsline {subsubsection}{Gr\'aficas}{48}{section*.32}
\contentsline {subsection}{\numberline {3.2.7}An\'alisis del efecto de la altura en el nuevo tipo de rotor}{50}{subsection.3.2.7}
\contentsline {subsubsection}{Tablas de datos}{50}{section*.33}
\contentsline {subsubsection}{Gr\'aficas}{51}{section*.34}
\contentsline {subsection}{\numberline {3.2.8}An\'alisis del efecto del \'angulo de las toberas en el nuevo rotor}{53}{subsection.3.2.8}
\contentsline {subsubsection}{Tablas de datos}{53}{section*.35}
\contentsline {subsubsection}{Gr\'aficas}{54}{section*.36}
\contentsline {subsection}{\numberline {3.2.9}An\'alisis del efecto del Encaje del Motor}{56}{subsection.3.2.9}
\contentsline {subsubsection}{Tablas de datos}{56}{section*.37}
\contentsline {subsubsection}{Gr\'aficas}{57}{section*.38}
\contentsline {subsection}{\numberline {3.2.10}An\'alisis del efecto de la Posici\'on del \'Alabe Secundario}{59}{subsection.3.2.10}
\contentsline {subsubsection}{Tablas de datos}{60}{section*.39}
\contentsline {subsubsection}{Gr\'aficas}{61}{section*.40}
\contentsline {section}{\numberline {3.3}Discusi\'on}{63}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Discusi\'on an\'alisis efecto del n\'umero de \'alabes}{63}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Discusi\'on an\'alisis efecto del uso de \'alabes interiores}{63}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Discusi\'on an\'alisis efecto de la altura del rotor}{63}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Discusi\'on an\'alisis efecto del \'angulo de los \'alabes}{64}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Discusi\'on an\'alisis efecto del \'angulo de las toberas}{65}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Discusi\'on an\'alisis efecto del uso de \'alabes dobles}{65}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Discusi\'on an\'alisis efecto de la altura en el nuevo tipo de rotor}{66}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Discusi\'on an\'alisis efecto del \'angulo de las toberas en el nuevo tipo de rotor}{66}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}Discusi\'on an\'alisis efecto del encaje del motor}{67}{subsection.3.3.9}
\contentsline {subsection}{\numberline {3.3.10}Discusi\'on an\'alisis efecto de la posici\'on del \'alabe secundario}{67}{subsection.3.3.10}
\contentsline {subsection}{\numberline {3.3.11}Discusi\'on Final}{69}{subsection.3.3.11}
\contentsline {subsection}{\numberline {3.3.12}Planos Dise\~no Final}{70}{subsection.3.3.12}
\contentsline {chapter}{\numberline {4}Dise\~no}{73}{chapter.4}
\contentsline {section}{\numberline {4.1}Concepto}{73}{section.4.1}
\contentsline {section}{\numberline {4.2}Dise\~no electr\'onico de la ventosa}{73}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}TCRT5000}{74}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}BMP280}{76}{subsection.4.2.2}
\contentsline {subsubsection}{Dise\~no con Regulador Lineal}{76}{section*.41}
\contentsline {subsubsection}{Dise\~no con circuito integrado}{78}{section*.42}
\contentsline {subsection}{\numberline {4.2.3}Motor Brushless}{80}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Variador ESC}{81}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}Dise\~no electr\'onico de la placa centralizadora}{81}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Microcontrolador}{82}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Multiplexador I2C}{83}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Esquem\'atico Circuito}{85}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Dise\~no mec\'anico}{87}{section.4.4}
\contentsline {chapter}{\numberline {5}Conclusiones y desarrollos futuros}{97}{chapter.5}
\contentsline {section}{\numberline {5.1}Conclusi\'on}{97}{section.5.1}
\contentsline {section}{\numberline {5.2}Desarrollos futuros}{98}{section.5.2}
\contentsline {chapter}{Bibliograf\IeC {\'\i }\-a}{99}{section.5.2}
\contentsline {chapter}{\numberline {A}Esquem\'aticos}{101}{appendix.Alph1}
\contentsline {chapter}{\numberline {B}Layout}{105}{appendix.Alph2}
\contentsline {chapter}{\numberline {C}Programaci\'on utilizada}{109}{appendix.Alph3}
\contentsline {section}{\numberline {C.1}Plataforma de pruebas}{109}{section.Alph3.1}
\contentsline {section}{\numberline {C.2}Multiplexador}{110}{section.Alph3.2}
\contentsline {section}{\numberline {C.3}Circuito Sensorizado}{112}{section.Alph3.3}
\contentsline {chapter}{\numberline {D}Software Utilizado}{115}{appendix.Alph4}
