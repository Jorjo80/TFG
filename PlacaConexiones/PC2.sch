<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.4.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="10" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="26" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="22-23-2051" library_version="1">
<description>.100" (2.54mm) Center Header - 5 Pin</description>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="-6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1" shape="long" rot="R90"/>
<text x="-6.35" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="22-23-2021" library_version="1">
<description>.100" (2.54mm) Center Headers - 2 Pin</description>
<wire x1="-2.54" y1="3.175" x2="2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.54" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="22-23-2031" library_version="1">
<description>.100" (2.54mm) Center Header - 3 Pin</description>
<wire x1="-3.81" y1="3.175" x2="3.81" y2="3.175" width="0.254" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="-3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="3.175" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<text x="-3.81" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MV" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22-23-2051" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 5 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2051">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2051" constant="no"/>
<attribute name="OC_FARNELL" value="1462952" constant="no"/>
<attribute name="OC_NEWARK" value="38C9178" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22-23-2021" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 2 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2021">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2021" constant="no"/>
<attribute name="OC_FARNELL" value="1462926" constant="no"/>
<attribute name="OC_NEWARK" value="25C3832" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22-23-2031" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 3 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2031">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2031" constant="no"/>
<attribute name="OC_FARNELL" value="1462950" constant="no"/>
<attribute name="OC_NEWARK" value="30C0862" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="solpad" urn="urn:adsk.eagle:library:364">
<description>&lt;b&gt;Solder Pads/Test Points&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LSP13" urn="urn:adsk.eagle:footprint:26496/1" library_version="1">
<description>&lt;b&gt;SOLDER PAD&lt;/b&gt;&lt;p&gt;
drill 1.3 mm</description>
<wire x1="-1.524" y1="0.254" x2="-1.524" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.254" x2="1.524" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.254" x2="1.143" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.254" x2="1.143" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.254" x2="-1.143" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.254" x2="0.635" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.254" x2="0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.254" x2="-0.635" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-0.254" x2="1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.254" x2="-0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-0.254" x2="0.635" y2="-0.254" width="0.1524" layer="51"/>
<pad name="MP" x="0" y="0" drill="1.3208" diameter="2.159" shape="octagon"/>
<text x="-1.524" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.254" size="0.0254" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LSP13" urn="urn:adsk.eagle:package:26509/1" type="box" library_version="1">
<description>SOLDER PAD
drill 1.3 mm</description>
</package3d>
</packages3d>
<symbols>
<symbol name="LSP" urn="urn:adsk.eagle:symbol:26492/1" library_version="1">
<wire x1="-1.016" y1="2.032" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="1.016" y2="2.032" width="0.254" layer="94"/>
<circle x="0" y="1.016" radius="1.016" width="0.4064" layer="94"/>
<text x="-1.27" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<pin name="MP" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LSP13" urn="urn:adsk.eagle:component:26514/1" prefix="LSP" library_version="1">
<description>&lt;b&gt;SOLDER PAD&lt;/b&gt;&lt;p&gt; drill 1.3 mm, distributor Buerklin, 12H562</description>
<gates>
<gate name="1" symbol="LSP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LSP13">
<connects>
<connect gate="1" pin="MP" pad="MP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26509/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="power" width="0.3048" drill="0">
</class>
</classes>
<parts>
<part name="PATA_0" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_2" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_3" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_4" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_5" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="BAT" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="BUS" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="5V150MA" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D8" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RX" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="TX" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RST" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RESET" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="TX2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RX2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="5V" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="AR" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D13" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D9" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D10" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D11" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D12" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="VIN" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SDA" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SDL" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="REST" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="BATERIA" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device=""/>
<part name="COMUNICACION" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device=""/>
<part name="ESC_0" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_2" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_4" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_5" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_3" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="PATA_0" gate="-1" x="276.86" y="325.12"/>
<instance part="PATA_0" gate="-2" x="276.86" y="322.58"/>
<instance part="PATA_0" gate="-3" x="276.86" y="320.04"/>
<instance part="PATA_0" gate="-4" x="276.86" y="317.5"/>
<instance part="PATA_0" gate="-5" x="276.86" y="314.96"/>
<instance part="PATA_1" gate="-1" x="299.72" y="325.12"/>
<instance part="PATA_1" gate="-2" x="299.72" y="322.58"/>
<instance part="PATA_1" gate="-3" x="299.72" y="320.04"/>
<instance part="PATA_1" gate="-4" x="299.72" y="317.5"/>
<instance part="PATA_1" gate="-5" x="299.72" y="314.96"/>
<instance part="PATA_2" gate="-1" x="251.46" y="325.12"/>
<instance part="PATA_2" gate="-2" x="251.46" y="322.58"/>
<instance part="PATA_2" gate="-3" x="251.46" y="320.04"/>
<instance part="PATA_2" gate="-4" x="251.46" y="317.5"/>
<instance part="PATA_2" gate="-5" x="251.46" y="314.96"/>
<instance part="PATA_3" gate="-1" x="231.14" y="325.12"/>
<instance part="PATA_3" gate="-2" x="231.14" y="322.58"/>
<instance part="PATA_3" gate="-3" x="231.14" y="320.04"/>
<instance part="PATA_3" gate="-4" x="231.14" y="317.5"/>
<instance part="PATA_3" gate="-5" x="231.14" y="314.96"/>
<instance part="PATA_4" gate="-1" x="320.04" y="325.12"/>
<instance part="PATA_4" gate="-2" x="320.04" y="322.58"/>
<instance part="PATA_4" gate="-3" x="320.04" y="320.04"/>
<instance part="PATA_4" gate="-4" x="320.04" y="317.5"/>
<instance part="PATA_4" gate="-5" x="320.04" y="314.96"/>
<instance part="PATA_5" gate="-1" x="337.82" y="322.58"/>
<instance part="PATA_5" gate="-2" x="337.82" y="320.04"/>
<instance part="PATA_5" gate="-3" x="337.82" y="317.5"/>
<instance part="PATA_5" gate="-4" x="337.82" y="314.96"/>
<instance part="PATA_5" gate="-5" x="337.82" y="312.42"/>
<instance part="BAT" gate="1" x="266.7" y="256.54"/>
<instance part="GND1" gate="1" x="276.86" y="254"/>
<instance part="BUS" gate="1" x="284.48" y="254"/>
<instance part="5V150MA" gate="1" x="292.1" y="254"/>
<instance part="D8" gate="1" x="304.8" y="254"/>
<instance part="D6" gate="1" x="314.96" y="254"/>
<instance part="D5" gate="1" x="330.2" y="254"/>
<instance part="D4" gate="1" x="337.82" y="256.54"/>
<instance part="D3" gate="1" x="345.44" y="254"/>
<instance part="RX" gate="1" x="355.6" y="254"/>
<instance part="TX" gate="1" x="360.68" y="254"/>
<instance part="RST" gate="1" x="365.76" y="254"/>
<instance part="RESET" gate="1" x="373.38" y="254"/>
<instance part="TX2" gate="1" x="388.62" y="251.46"/>
<instance part="RX2" gate="1" x="396.24" y="254"/>
<instance part="5V" gate="1" x="403.86" y="254"/>
<instance part="GND2" gate="1" x="408.94" y="254"/>
<instance part="GND3" gate="1" x="416.56" y="254"/>
<instance part="A5" gate="1" x="421.64" y="254"/>
<instance part="A4" gate="1" x="426.72" y="254"/>
<instance part="A3" gate="1" x="431.8" y="254"/>
<instance part="A2" gate="1" x="436.88" y="254"/>
<instance part="A1" gate="1" x="444.5" y="254"/>
<instance part="A0" gate="1" x="449.58" y="251.46"/>
<instance part="AR" gate="1" x="454.66" y="251.46"/>
<instance part="D13" gate="1" x="462.28" y="251.46"/>
<instance part="D9" gate="1" x="459.74" y="238.76"/>
<instance part="D10" gate="1" x="452.12" y="236.22"/>
<instance part="D11" gate="1" x="439.42" y="236.22"/>
<instance part="D12" gate="1" x="426.72" y="238.76"/>
<instance part="A7" gate="1" x="365.76" y="241.3"/>
<instance part="A6" gate="1" x="353.06" y="241.3"/>
<instance part="VIN" gate="1" x="246.38" y="381"/>
<instance part="GND" gate="1" x="259.08" y="381"/>
<instance part="SDA" gate="1" x="274.32" y="381"/>
<instance part="SDL" gate="1" x="284.48" y="381"/>
<instance part="REST" gate="1" x="299.72" y="381"/>
<instance part="A_0" gate="1" x="309.88" y="381"/>
<instance part="A_1" gate="1" x="314.96" y="381"/>
<instance part="A_2" gate="1" x="322.58" y="381"/>
<instance part="SD0" gate="1" x="335.28" y="381"/>
<instance part="SC0" gate="1" x="347.98" y="381"/>
<instance part="SD1" gate="1" x="358.14" y="381"/>
<instance part="SC1" gate="1" x="368.3" y="381"/>
<instance part="SC7" gate="1" x="243.84" y="355.6"/>
<instance part="SD7" gate="1" x="259.08" y="358.14"/>
<instance part="SC6" gate="1" x="271.78" y="358.14"/>
<instance part="SD6" gate="1" x="281.94" y="358.14"/>
<instance part="SC5" gate="1" x="292.1" y="358.14"/>
<instance part="SD5" gate="1" x="304.8" y="360.68"/>
<instance part="SC4" gate="1" x="312.42" y="358.14"/>
<instance part="SD4" gate="1" x="325.12" y="358.14"/>
<instance part="SC3" gate="1" x="330.2" y="355.6"/>
<instance part="SD3" gate="1" x="337.82" y="355.6"/>
<instance part="SC2" gate="1" x="347.98" y="358.14"/>
<instance part="SD2" gate="1" x="355.6" y="358.14"/>
<instance part="BATERIA" gate="-1" x="210.82" y="398.78"/>
<instance part="BATERIA" gate="-2" x="210.82" y="396.24"/>
<instance part="COMUNICACION" gate="-1" x="210.82" y="388.62"/>
<instance part="COMUNICACION" gate="-2" x="210.82" y="386.08"/>
<instance part="ESC_0" gate="-1" x="370.84" y="320.04"/>
<instance part="ESC_0" gate="-2" x="370.84" y="317.5"/>
<instance part="ESC_0" gate="-3" x="370.84" y="314.96"/>
<instance part="ESC_1" gate="-1" x="386.08" y="317.5"/>
<instance part="ESC_1" gate="-2" x="386.08" y="314.96"/>
<instance part="ESC_1" gate="-3" x="386.08" y="312.42"/>
<instance part="ESC_2" gate="-1" x="414.02" y="317.5"/>
<instance part="ESC_2" gate="-2" x="414.02" y="314.96"/>
<instance part="ESC_2" gate="-3" x="414.02" y="312.42"/>
<instance part="ESC_4" gate="-1" x="449.58" y="317.5"/>
<instance part="ESC_4" gate="-2" x="449.58" y="314.96"/>
<instance part="ESC_4" gate="-3" x="449.58" y="312.42"/>
<instance part="ESC_5" gate="-1" x="469.9" y="317.5"/>
<instance part="ESC_5" gate="-2" x="469.9" y="314.96"/>
<instance part="ESC_5" gate="-3" x="469.9" y="312.42"/>
<instance part="ESC_3" gate="-1" x="434.34" y="317.5"/>
<instance part="ESC_3" gate="-2" x="434.34" y="314.96"/>
<instance part="ESC_3" gate="-3" x="434.34" y="312.42"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$6" class="0">
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D3" gate="1" pin="MP"/>
<wire x1="368.3" y1="314.96" x2="345.44" y2="314.96" width="0.1524" layer="91"/>
<wire x1="345.44" y1="314.96" x2="345.44" y2="251.46" width="0.1524" layer="91"/>
<pinref part="ESC_0" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="383.54" y1="312.42" x2="383.54" y2="279.4" width="0.1524" layer="91"/>
<wire x1="383.54" y1="279.4" x2="386.08" y2="279.4" width="0.1524" layer="91"/>
<wire x1="386.08" y1="279.4" x2="386.08" y2="271.78" width="0.1524" layer="91"/>
<wire x1="386.08" y1="271.78" x2="327.66" y2="271.78" width="0.1524" layer="91"/>
<pinref part="D5" gate="1" pin="MP"/>
<wire x1="327.66" y1="271.78" x2="327.66" y2="251.46" width="0.1524" layer="91"/>
<wire x1="327.66" y1="251.46" x2="330.2" y2="251.46" width="0.1524" layer="91"/>
<pinref part="ESC_1" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="411.48" y1="312.42" x2="411.48" y2="264.16" width="0.1524" layer="91"/>
<wire x1="411.48" y1="264.16" x2="314.96" y2="264.16" width="0.1524" layer="91"/>
<pinref part="D6" gate="1" pin="MP"/>
<wire x1="314.96" y1="264.16" x2="314.96" y2="251.46" width="0.1524" layer="91"/>
<pinref part="ESC_2" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="431.8" y1="312.42" x2="431.8" y2="264.16" width="0.1524" layer="91"/>
<wire x1="431.8" y1="264.16" x2="464.82" y2="264.16" width="0.1524" layer="91"/>
<pinref part="D9" gate="1" pin="MP"/>
<wire x1="464.82" y1="264.16" x2="464.82" y2="236.22" width="0.1524" layer="91"/>
<wire x1="464.82" y1="236.22" x2="459.74" y2="236.22" width="0.1524" layer="91"/>
<pinref part="ESC_3" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="D10" gate="1" pin="MP"/>
<wire x1="452.12" y1="233.68" x2="447.04" y2="233.68" width="0.1524" layer="91"/>
<wire x1="447.04" y1="233.68" x2="447.04" y2="312.42" width="0.1524" layer="91"/>
<pinref part="ESC_4" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="467.36" y1="312.42" x2="467.36" y2="226.06" width="0.1524" layer="91"/>
<wire x1="467.36" y1="226.06" x2="439.42" y2="226.06" width="0.1524" layer="91"/>
<pinref part="D11" gate="1" pin="MP"/>
<wire x1="439.42" y1="226.06" x2="439.42" y2="233.68" width="0.1524" layer="91"/>
<pinref part="ESC_5" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="GND2" gate="1" pin="MP"/>
<pinref part="GND3" gate="1" pin="MP"/>
<wire x1="408.94" y1="251.46" x2="416.56" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_150" class="0">
<segment>
<pinref part="PATA_3" gate="-1" pin="S"/>
<pinref part="PATA_2" gate="-1" pin="S"/>
<wire x1="228.6" y1="325.12" x2="248.92" y2="325.12" width="0.1524" layer="91"/>
<pinref part="PATA_0" gate="-1" pin="S"/>
<wire x1="248.92" y1="325.12" x2="274.32" y2="325.12" width="0.1524" layer="91"/>
<junction x="248.92" y="325.12"/>
<pinref part="PATA_1" gate="-1" pin="S"/>
<wire x1="274.32" y1="325.12" x2="297.18" y2="325.12" width="0.1524" layer="91"/>
<junction x="274.32" y="325.12"/>
<pinref part="PATA_4" gate="-1" pin="S"/>
<wire x1="297.18" y1="325.12" x2="317.5" y2="325.12" width="0.1524" layer="91"/>
<junction x="297.18" y="325.12"/>
<pinref part="PATA_5" gate="-1" pin="S"/>
<wire x1="317.5" y1="325.12" x2="335.28" y2="325.12" width="0.1524" layer="91"/>
<wire x1="335.28" y1="325.12" x2="335.28" y2="322.58" width="0.1524" layer="91"/>
<junction x="317.5" y="325.12"/>
<wire x1="228.6" y1="325.12" x2="220.98" y2="325.12" width="0.1524" layer="91"/>
<junction x="228.6" y="325.12"/>
<wire x1="220.98" y1="325.12" x2="220.98" y2="266.7" width="0.1524" layer="91"/>
<wire x1="220.98" y1="266.7" x2="292.1" y2="266.7" width="0.1524" layer="91"/>
<pinref part="5V150MA" gate="1" pin="MP"/>
<wire x1="292.1" y1="266.7" x2="292.1" y2="251.46" width="0.1524" layer="91"/>
<pinref part="VIN" gate="1" pin="MP"/>
<wire x1="246.38" y1="378.46" x2="193.04" y2="378.46" width="0.1524" layer="91"/>
<wire x1="193.04" y1="378.46" x2="193.04" y2="233.68" width="0.1524" layer="91"/>
<wire x1="193.04" y1="233.68" x2="195.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="195.58" y1="233.68" x2="195.58" y2="231.14" width="0.1524" layer="91"/>
<wire x1="195.58" y1="231.14" x2="292.1" y2="231.14" width="0.1524" layer="91"/>
<wire x1="292.1" y1="231.14" x2="292.1" y2="251.46" width="0.1524" layer="91"/>
<junction x="292.1" y="251.46"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="PATA_3" gate="-2" pin="S"/>
<wire x1="228.6" y1="322.58" x2="210.82" y2="322.58" width="0.1524" layer="91"/>
<wire x1="210.82" y1="322.58" x2="210.82" y2="246.38" width="0.1524" layer="91"/>
<wire x1="210.82" y1="246.38" x2="431.8" y2="246.38" width="0.1524" layer="91"/>
<pinref part="A3" gate="1" pin="MP"/>
<wire x1="431.8" y1="246.38" x2="431.8" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="PATA_2" gate="-2" pin="S"/>
<wire x1="248.92" y1="322.58" x2="241.3" y2="322.58" width="0.1524" layer="91"/>
<wire x1="241.3" y1="322.58" x2="241.3" y2="276.86" width="0.1524" layer="91"/>
<wire x1="241.3" y1="276.86" x2="436.88" y2="276.86" width="0.1524" layer="91"/>
<pinref part="A2" gate="1" pin="MP"/>
<wire x1="436.88" y1="276.86" x2="436.88" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="PATA_0" gate="-2" pin="S"/>
<wire x1="274.32" y1="322.58" x2="269.24" y2="322.58" width="0.1524" layer="91"/>
<wire x1="269.24" y1="322.58" x2="269.24" y2="281.94" width="0.1524" layer="91"/>
<wire x1="269.24" y1="281.94" x2="452.12" y2="281.94" width="0.1524" layer="91"/>
<pinref part="A0" gate="1" pin="MP"/>
<wire x1="452.12" y1="281.94" x2="452.12" y2="248.92" width="0.1524" layer="91"/>
<wire x1="452.12" y1="248.92" x2="449.58" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="PATA_1" gate="-2" pin="S"/>
<wire x1="297.18" y1="322.58" x2="292.1" y2="322.58" width="0.1524" layer="91"/>
<wire x1="292.1" y1="322.58" x2="292.1" y2="287.02" width="0.1524" layer="91"/>
<wire x1="292.1" y1="287.02" x2="441.96" y2="287.02" width="0.1524" layer="91"/>
<pinref part="A1" gate="1" pin="MP"/>
<wire x1="441.96" y1="287.02" x2="441.96" y2="251.46" width="0.1524" layer="91"/>
<wire x1="441.96" y1="251.46" x2="444.5" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="PATA_4" gate="-2" pin="S"/>
<wire x1="317.5" y1="322.58" x2="312.42" y2="322.58" width="0.1524" layer="91"/>
<pinref part="A6" gate="1" pin="MP"/>
<wire x1="312.42" y1="322.58" x2="312.42" y2="238.76" width="0.1524" layer="91"/>
<wire x1="312.42" y1="238.76" x2="353.06" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="PATA_5" gate="-2" pin="S"/>
<wire x1="335.28" y1="320.04" x2="330.2" y2="320.04" width="0.1524" layer="91"/>
<wire x1="330.2" y1="320.04" x2="330.2" y2="294.64" width="0.1524" layer="91"/>
<wire x1="330.2" y1="294.64" x2="322.58" y2="294.64" width="0.1524" layer="91"/>
<wire x1="322.58" y1="294.64" x2="322.58" y2="236.22" width="0.1524" layer="91"/>
<wire x1="322.58" y1="236.22" x2="365.76" y2="236.22" width="0.1524" layer="91"/>
<pinref part="A7" gate="1" pin="MP"/>
<wire x1="365.76" y1="236.22" x2="365.76" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="PATA_3" gate="-4" pin="S"/>
<wire x1="228.6" y1="317.5" x2="226.06" y2="317.5" width="0.1524" layer="91"/>
<wire x1="226.06" y1="317.5" x2="226.06" y2="345.44" width="0.1524" layer="91"/>
<wire x1="226.06" y1="345.44" x2="330.2" y2="345.44" width="0.1524" layer="91"/>
<pinref part="SC3" gate="1" pin="MP"/>
<wire x1="330.2" y1="345.44" x2="330.2" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="PATA_3" gate="-3" pin="S"/>
<wire x1="228.6" y1="320.04" x2="203.2" y2="320.04" width="0.1524" layer="91"/>
<wire x1="203.2" y1="320.04" x2="203.2" y2="337.82" width="0.1524" layer="91"/>
<wire x1="203.2" y1="337.82" x2="337.82" y2="337.82" width="0.1524" layer="91"/>
<pinref part="SD3" gate="1" pin="MP"/>
<wire x1="337.82" y1="337.82" x2="337.82" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="PATA_2" gate="-3" pin="S"/>
<wire x1="248.92" y1="320.04" x2="246.38" y2="320.04" width="0.1524" layer="91"/>
<wire x1="246.38" y1="320.04" x2="246.38" y2="335.28" width="0.1524" layer="91"/>
<wire x1="246.38" y1="335.28" x2="355.6" y2="335.28" width="0.1524" layer="91"/>
<pinref part="SD2" gate="1" pin="MP"/>
<wire x1="355.6" y1="335.28" x2="355.6" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="PATA_2" gate="-4" pin="S"/>
<wire x1="248.92" y1="317.5" x2="243.84" y2="317.5" width="0.1524" layer="91"/>
<wire x1="243.84" y1="317.5" x2="243.84" y2="332.74" width="0.1524" layer="91"/>
<wire x1="243.84" y1="332.74" x2="350.52" y2="332.74" width="0.1524" layer="91"/>
<pinref part="SC2" gate="1" pin="MP"/>
<wire x1="350.52" y1="332.74" x2="350.52" y2="355.6" width="0.1524" layer="91"/>
<wire x1="350.52" y1="355.6" x2="347.98" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="PATA_0" gate="-3" pin="S"/>
<wire x1="274.32" y1="320.04" x2="264.16" y2="320.04" width="0.1524" layer="91"/>
<wire x1="264.16" y1="320.04" x2="264.16" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SD0" gate="1" pin="MP"/>
<wire x1="264.16" y1="393.7" x2="335.28" y2="393.7" width="0.1524" layer="91"/>
<wire x1="335.28" y1="393.7" x2="335.28" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="PATA_0" gate="-4" pin="S"/>
<wire x1="274.32" y1="317.5" x2="261.62" y2="317.5" width="0.1524" layer="91"/>
<wire x1="261.62" y1="317.5" x2="261.62" y2="398.78" width="0.1524" layer="91"/>
<wire x1="261.62" y1="398.78" x2="347.98" y2="398.78" width="0.1524" layer="91"/>
<pinref part="SC0" gate="1" pin="MP"/>
<wire x1="347.98" y1="398.78" x2="347.98" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="PATA_1" gate="-3" pin="S"/>
<wire x1="297.18" y1="320.04" x2="287.02" y2="320.04" width="0.1524" layer="91"/>
<wire x1="287.02" y1="320.04" x2="287.02" y2="373.38" width="0.1524" layer="91"/>
<pinref part="SD1" gate="1" pin="MP"/>
<wire x1="287.02" y1="373.38" x2="358.14" y2="373.38" width="0.1524" layer="91"/>
<wire x1="358.14" y1="373.38" x2="358.14" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="PATA_1" gate="-4" pin="S"/>
<wire x1="297.18" y1="317.5" x2="289.56" y2="317.5" width="0.1524" layer="91"/>
<wire x1="289.56" y1="317.5" x2="289.56" y2="350.52" width="0.1524" layer="91"/>
<wire x1="289.56" y1="350.52" x2="368.3" y2="350.52" width="0.1524" layer="91"/>
<pinref part="SC1" gate="1" pin="MP"/>
<wire x1="368.3" y1="350.52" x2="368.3" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="PATA_4" gate="-3" pin="S"/>
<wire x1="317.5" y1="320.04" x2="304.8" y2="320.04" width="0.1524" layer="91"/>
<wire x1="304.8" y1="320.04" x2="304.8" y2="353.06" width="0.1524" layer="91"/>
<wire x1="304.8" y1="353.06" x2="325.12" y2="353.06" width="0.1524" layer="91"/>
<pinref part="SD4" gate="1" pin="MP"/>
<wire x1="325.12" y1="353.06" x2="325.12" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="PATA_4" gate="-4" pin="S"/>
<wire x1="317.5" y1="317.5" x2="309.88" y2="317.5" width="0.1524" layer="91"/>
<wire x1="309.88" y1="317.5" x2="309.88" y2="355.6" width="0.1524" layer="91"/>
<pinref part="SC4" gate="1" pin="MP"/>
<wire x1="309.88" y1="355.6" x2="312.42" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="PATA_5" gate="-3" pin="S"/>
<wire x1="335.28" y1="317.5" x2="332.74" y2="317.5" width="0.1524" layer="91"/>
<wire x1="332.74" y1="317.5" x2="332.74" y2="342.9" width="0.1524" layer="91"/>
<wire x1="332.74" y1="342.9" x2="299.72" y2="342.9" width="0.1524" layer="91"/>
<wire x1="299.72" y1="342.9" x2="299.72" y2="358.14" width="0.1524" layer="91"/>
<pinref part="SD5" gate="1" pin="MP"/>
<wire x1="299.72" y1="358.14" x2="304.8" y2="358.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="PATA_5" gate="-4" pin="S"/>
<wire x1="335.28" y1="314.96" x2="342.9" y2="314.96" width="0.1524" layer="91"/>
<wire x1="342.9" y1="314.96" x2="342.9" y2="340.36" width="0.1524" layer="91"/>
<wire x1="342.9" y1="340.36" x2="292.1" y2="340.36" width="0.1524" layer="91"/>
<pinref part="SC5" gate="1" pin="MP"/>
<wire x1="292.1" y1="340.36" x2="292.1" y2="355.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="SDA" gate="1" pin="MP"/>
<wire x1="274.32" y1="378.46" x2="266.7" y2="378.46" width="0.1524" layer="91"/>
<wire x1="266.7" y1="378.46" x2="266.7" y2="411.48" width="0.1524" layer="91"/>
<wire x1="266.7" y1="411.48" x2="477.52" y2="411.48" width="0.1524" layer="91"/>
<wire x1="477.52" y1="411.48" x2="477.52" y2="243.84" width="0.1524" layer="91"/>
<wire x1="477.52" y1="243.84" x2="426.72" y2="243.84" width="0.1524" layer="91"/>
<pinref part="A4" gate="1" pin="MP"/>
<wire x1="426.72" y1="243.84" x2="426.72" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="A5" gate="1" pin="MP"/>
<wire x1="424.18" y1="251.46" x2="421.64" y2="251.46" width="0.1524" layer="91"/>
<pinref part="SDL" gate="1" pin="MP"/>
<wire x1="421.64" y1="251.46" x2="421.64" y2="406.4" width="0.1524" layer="91"/>
<wire x1="421.64" y1="406.4" x2="284.48" y2="406.4" width="0.1524" layer="91"/>
<wire x1="284.48" y1="406.4" x2="284.48" y2="378.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="416.56" y1="256.54" x2="419.1" y2="256.54" width="0.1524" layer="91"/>
<wire x1="419.1" y1="256.54" x2="419.1" y2="317.5" width="0.1524" layer="91"/>
<wire x1="419.1" y1="317.5" x2="411.48" y2="317.5" width="0.1524" layer="91"/>
<wire x1="411.48" y1="317.5" x2="398.78" y2="317.5" width="0.1524" layer="91"/>
<wire x1="373.38" y1="320.04" x2="398.78" y2="320.04" width="0.1524" layer="91"/>
<wire x1="398.78" y1="320.04" x2="398.78" y2="317.5" width="0.1524" layer="91"/>
<junction x="419.1" y="317.5"/>
<wire x1="419.1" y1="317.5" x2="431.8" y2="317.5" width="0.1524" layer="91"/>
<wire x1="431.8" y1="317.5" x2="447.04" y2="317.5" width="0.1524" layer="91"/>
<wire x1="447.04" y1="317.5" x2="467.36" y2="317.5" width="0.1524" layer="91"/>
<wire x1="373.38" y1="320.04" x2="373.38" y2="302.26" width="0.1524" layer="91"/>
<pinref part="PATA_3" gate="-5" pin="S"/>
<pinref part="PATA_2" gate="-5" pin="S"/>
<wire x1="228.6" y1="314.96" x2="248.92" y2="314.96" width="0.1524" layer="91"/>
<pinref part="PATA_0" gate="-5" pin="S"/>
<wire x1="248.92" y1="314.96" x2="274.32" y2="314.96" width="0.1524" layer="91"/>
<junction x="248.92" y="314.96"/>
<pinref part="PATA_1" gate="-5" pin="S"/>
<wire x1="274.32" y1="314.96" x2="297.18" y2="314.96" width="0.1524" layer="91"/>
<junction x="274.32" y="314.96"/>
<pinref part="PATA_4" gate="-5" pin="S"/>
<wire x1="297.18" y1="314.96" x2="317.5" y2="314.96" width="0.1524" layer="91"/>
<junction x="297.18" y="314.96"/>
<wire x1="317.5" y1="314.96" x2="332.74" y2="314.96" width="0.1524" layer="91"/>
<wire x1="332.74" y1="314.96" x2="332.74" y2="312.42" width="0.1524" layer="91"/>
<junction x="317.5" y="314.96"/>
<pinref part="PATA_5" gate="-5" pin="S"/>
<wire x1="332.74" y1="312.42" x2="335.28" y2="312.42" width="0.1524" layer="91"/>
<wire x1="228.6" y1="314.96" x2="228.6" y2="261.62" width="0.1524" layer="91"/>
<wire x1="228.6" y1="261.62" x2="276.86" y2="261.62" width="0.1524" layer="91"/>
<junction x="228.6" y="314.96"/>
<pinref part="GND1" gate="1" pin="MP"/>
<wire x1="276.86" y1="261.62" x2="276.86" y2="251.46" width="0.1524" layer="91"/>
<pinref part="GND" gate="1" pin="MP"/>
<wire x1="259.08" y1="378.46" x2="259.08" y2="365.76" width="0.1524" layer="91"/>
<wire x1="259.08" y1="365.76" x2="198.12" y2="365.76" width="0.1524" layer="91"/>
<wire x1="198.12" y1="365.76" x2="198.12" y2="241.3" width="0.1524" layer="91"/>
<wire x1="198.12" y1="241.3" x2="276.86" y2="241.3" width="0.1524" layer="91"/>
<wire x1="276.86" y1="241.3" x2="276.86" y2="251.46" width="0.1524" layer="91"/>
<junction x="276.86" y="251.46"/>
<wire x1="373.38" y1="302.26" x2="276.86" y2="302.26" width="0.1524" layer="91"/>
<wire x1="276.86" y1="302.26" x2="276.86" y2="251.46" width="0.1524" layer="91"/>
<pinref part="BATERIA" gate="-2" pin="S"/>
<wire x1="208.28" y1="396.24" x2="185.42" y2="396.24" width="0.1524" layer="91"/>
<wire x1="185.42" y1="396.24" x2="185.42" y2="226.06" width="0.1524" layer="91"/>
<wire x1="185.42" y1="226.06" x2="279.4" y2="226.06" width="0.1524" layer="91"/>
<wire x1="279.4" y1="226.06" x2="279.4" y2="241.3" width="0.1524" layer="91"/>
<wire x1="279.4" y1="241.3" x2="276.86" y2="241.3" width="0.1524" layer="91"/>
<junction x="276.86" y="241.3"/>
<pinref part="ESC_5" gate="-1" pin="S"/>
<pinref part="ESC_4" gate="-1" pin="S"/>
<junction x="447.04" y="317.5"/>
<pinref part="ESC_3" gate="-1" pin="S"/>
<junction x="431.8" y="317.5"/>
<pinref part="ESC_2" gate="-1" pin="S"/>
<junction x="411.48" y="317.5"/>
<pinref part="ESC_1" gate="-1" pin="S"/>
<wire x1="383.54" y1="317.5" x2="398.78" y2="317.5" width="0.1524" layer="91"/>
<junction x="398.78" y="317.5"/>
<pinref part="ESC_0" gate="-1" pin="S"/>
<wire x1="368.3" y1="320.04" x2="373.38" y2="320.04" width="0.1524" layer="91"/>
<junction x="373.38" y="320.04"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="TX" gate="1" pin="MP"/>
<wire x1="360.68" y1="251.46" x2="360.68" y2="248.92" width="0.1524" layer="91"/>
<wire x1="360.68" y1="248.92" x2="203.2" y2="248.92" width="0.1524" layer="91"/>
<wire x1="203.2" y1="248.92" x2="203.2" y2="388.62" width="0.1524" layer="91"/>
<pinref part="COMUNICACION" gate="-1" pin="S"/>
<wire x1="203.2" y1="388.62" x2="208.28" y2="388.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BATERIA+" class="0">
<segment>
<pinref part="BAT" gate="1" pin="MP"/>
<wire x1="266.7" y1="254" x2="215.9" y2="254" width="0.1524" layer="91"/>
<wire x1="215.9" y1="254" x2="215.9" y2="398.78" width="0.1524" layer="91"/>
<pinref part="BATERIA" gate="-1" pin="S"/>
<wire x1="215.9" y1="398.78" x2="208.28" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="RX" gate="1" pin="MP"/>
<wire x1="355.6" y1="251.46" x2="355.6" y2="218.44" width="0.1524" layer="91"/>
<wire x1="355.6" y1="218.44" x2="205.74" y2="218.44" width="0.1524" layer="91"/>
<pinref part="COMUNICACION" gate="-2" pin="S"/>
<wire x1="205.74" y1="218.44" x2="205.74" y2="386.08" width="0.1524" layer="91"/>
<wire x1="205.74" y1="386.08" x2="208.28" y2="386.08" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
