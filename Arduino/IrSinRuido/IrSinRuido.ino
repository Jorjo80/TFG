float amaximo=1023; // 5 cm
float aminimo=0; // 1.5 cm
float distmin=0;
float distmax=12;
float distancia;
int TCRT = A3;
int TCRTLED = 6;
int a,b,c;
void setup() 
{
Serial.begin(9600);
pinMode(6,OUTPUT);
}

void loop() 
{
 digitalWrite(6,HIGH);    // Encendemos LED
 delay(500);  //wait
 a=analogRead(A3);        //Leemos señal + ruido
 digitalWrite(6,LOW);     //apagamos led
 delay(500);  //wait
 b=analogRead(A3);        // leemos ruido
 c=b-a;                    // (señal + ruido) - ruido = señal
 Serial.print("a = ");
 Serial.println(a);
 Serial.print("b = ");
 Serial.println(b);
 Serial.println(c); // señal sin ruido


 
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  float valor;
  valor=(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  return valor;
}
