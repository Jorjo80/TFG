float amaximo=984.0; // 4.0 cm
float aminimo=284.0; // 1.0 cm
float distmin=1.0;
float distmax=4.0;
void setup() {
 pinMode(A3,INPUT);
 Serial.begin(9600);

}

void loop() {
  float lectura=0.0;
  float distancia;
  lectura=analogRead(A3);
  distancia = fmap(lectura,aminimo,amaximo,distmin,distmax);
  Serial.println(lectura);
  Serial.print(distancia);
  Serial.println("cm");
  Serial.println();
  delay(500);
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  float valor;
  valor=(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  return valor;
}
