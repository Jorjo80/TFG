#include <SFE_BMP180.h>
#include <Wire.h>

volatile int contador = 0;   // Variable entera que se almacena en la RAM del Micro 
SFE_BMP180 bmp180;
 
void setup()

{
  Serial.begin(9600);
 
  if (bmp180.begin())
    Serial.println("MONITOR INICIADO");
  else
  {
    Serial.println("Error al iniciar BMP180");
    while(1); // bucle infinito
  }
  attachInterrupt(0,interrupcion0,RISING);  // Interrupcion 0 (pin2) 
}
 
void loop()
{
  char status;
  double T,P;
 
  status = bmp180.startTemperature(); //Inicio de lectura de temperatura
  if (status != 0)
  {   
    delay(status); //Pausa para que finalice la lectura
    status = bmp180.getTemperature(T); //Obtener la temperatura
    if (status != 0)
    {
      status = bmp180.startPressure(3); //Inicio lectura de presión
      if (status != 0)
      {        
        delay(status); //Pausa para que finalice la lectura        
        status = bmp180.getPressure(P,T); //Obtenemos la presión
        if (status != 0)
        {                  
          //Serial.print("Temperatura: ");
          //Serial.print(T,2);
          //Serial.print(" Grados C. , ");
          Serial.print("Presion: ");
          Serial.print(P,2);
          Serial.println(" mbar");          
        }      
      }      
    }   
  } 
  delay(999);               // retardo de casi 1 segundo
  Serial.print(contador*30); // Como son dos interrupciones por vuelta (contador * (60/2))
  Serial.println(" RPM");    //  El numero 2 depende del numero aspas de la helise del motor en prueba
  contador = 0;
}
void interrupcion0()    // Funcion que se ejecuta durante cada interrupion
{
  contador++;           // Se incrementa en uno el contador
}
