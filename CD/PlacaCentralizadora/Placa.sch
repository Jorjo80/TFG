<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="10" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="26" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="22-23-2051" library_version="1">
<description>.100" (2.54mm) Center Header - 5 Pin</description>
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="-6.35" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="-3.175" x2="-6.35" y2="1.27" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="3.175" width="0.254" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="6.35" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1" shape="long" rot="R90"/>
<text x="-6.35" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="22-23-2021" library_version="1">
<description>.100" (2.54mm) Center Headers - 2 Pin</description>
<wire x1="-2.54" y1="3.175" x2="2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="3.175" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1" shape="long" rot="R90"/>
<text x="-2.54" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="22-23-2031" library_version="1">
<description>.100" (2.54mm) Center Header - 3 Pin</description>
<wire x1="-3.81" y1="3.175" x2="3.81" y2="3.175" width="0.254" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="-3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="3.175" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<text x="-3.81" y="3.81" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MV" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" library_version="1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22-23-2051" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 5 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2051">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2051" constant="no"/>
<attribute name="OC_FARNELL" value="1462952" constant="no"/>
<attribute name="OC_NEWARK" value="38C9178" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22-23-2021" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 2 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2021">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2021" constant="no"/>
<attribute name="OC_FARNELL" value="1462926" constant="no"/>
<attribute name="OC_NEWARK" value="25C3832" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22-23-2031" prefix="X" library_version="1">
<description>.100" (2.54mm) Center Header - 3 Pin</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="22-23-2031">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="22-23-2031" constant="no"/>
<attribute name="OC_FARNELL" value="1462950" constant="no"/>
<attribute name="OC_NEWARK" value="30C0862" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="solpad" urn="urn:adsk.eagle:library:364">
<description>&lt;b&gt;Solder Pads/Test Points&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LSP13" urn="urn:adsk.eagle:footprint:26496/1" library_version="1">
<description>&lt;b&gt;SOLDER PAD&lt;/b&gt;&lt;p&gt;
drill 1.3 mm</description>
<wire x1="-1.524" y1="0.254" x2="-1.524" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.254" x2="1.524" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.254" x2="1.143" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.254" x2="1.143" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.254" x2="-1.143" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.254" x2="0.635" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.254" x2="0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0.254" x2="-0.635" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-0.254" x2="1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.254" x2="-0.635" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-0.254" x2="0.635" y2="-0.254" width="0.1524" layer="51"/>
<pad name="MP" x="0" y="0" drill="1.3208" diameter="2.159" shape="octagon"/>
<text x="-1.524" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.254" size="0.0254" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LSP13" urn="urn:adsk.eagle:package:26509/1" type="box" library_version="1">
<description>SOLDER PAD
drill 1.3 mm</description>
</package3d>
</packages3d>
<symbols>
<symbol name="LSP" urn="urn:adsk.eagle:symbol:26492/1" library_version="1">
<wire x1="-1.016" y1="2.032" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="1.016" y2="2.032" width="0.254" layer="94"/>
<circle x="0" y="1.016" radius="1.016" width="0.4064" layer="94"/>
<text x="-1.27" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<pin name="MP" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LSP13" urn="urn:adsk.eagle:component:26514/1" prefix="LSP" library_version="1">
<description>&lt;b&gt;SOLDER PAD&lt;/b&gt;&lt;p&gt; drill 1.3 mm, distributor Buerklin, 12H562</description>
<gates>
<gate name="1" symbol="LSP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LSP13">
<connects>
<connect gate="1" pin="MP" pad="MP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26509/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="V+" urn="urn:adsk.eagle:symbol:26939/1" library_version="1">
<wire x1="0.889" y1="-1.27" x2="0" y2="0.127" width="0.254" layer="94"/>
<wire x1="0" y1="0.127" x2="-0.889" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-1.27" x2="0.889" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="V+" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="V+" urn="urn:adsk.eagle:component:26966/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="V+" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="power" width="0.3048" drill="0">
</class>
</classes>
<parts>
<part name="PATA_0" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_2" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_3" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_4" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="PATA_5" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2051" device=""/>
<part name="BAT" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="BUS" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="5V150MA" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D8" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RX" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="TX" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RST" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RESET" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="TX2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="RX2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="5V" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="AR" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D13" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D9" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D10" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D11" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="D12" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="VIN" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="GND" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SDA" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SDL" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="REST" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="A_2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC0" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC1" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD7" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD6" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD5" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD4" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD3" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SC2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="SD2" library="solpad" library_urn="urn:adsk.eagle:library:364" deviceset="LSP13" device="" package3d_urn="urn:adsk.eagle:package:26509/1"/>
<part name="BATERIA" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device=""/>
<part name="COMUNICACION" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2021" device=""/>
<part name="ESC_0" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_1" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_2" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_4" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_5" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="ESC_3" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="22-23-2031" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="V+" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="276.86" y="355.6" size="1.778" layer="91">Variadores ESC</text>
<text x="271.78" y="299.72" size="1.778" layer="91">Circuitos Sensorizado</text>
<text x="279.4" y="226.06" size="1.778" layer="91">Pro Trinket 5V</text>
<text x="325.12" y="228.6" size="1.778" layer="91">Multiplexador I2C</text>
</plain>
<instances>
<instance part="PATA_0" gate="-1" x="266.7" y="325.12"/>
<instance part="PATA_0" gate="-2" x="266.7" y="322.58"/>
<instance part="PATA_0" gate="-3" x="266.7" y="320.04"/>
<instance part="PATA_0" gate="-4" x="266.7" y="317.5"/>
<instance part="PATA_0" gate="-5" x="266.7" y="314.96"/>
<instance part="PATA_1" gate="-1" x="304.8" y="325.12"/>
<instance part="PATA_1" gate="-2" x="304.8" y="322.58"/>
<instance part="PATA_1" gate="-3" x="304.8" y="320.04"/>
<instance part="PATA_1" gate="-4" x="304.8" y="317.5"/>
<instance part="PATA_1" gate="-5" x="304.8" y="314.96"/>
<instance part="PATA_2" gate="-1" x="231.14" y="325.12"/>
<instance part="PATA_2" gate="-2" x="231.14" y="322.58"/>
<instance part="PATA_2" gate="-3" x="231.14" y="320.04"/>
<instance part="PATA_2" gate="-4" x="231.14" y="317.5"/>
<instance part="PATA_2" gate="-5" x="231.14" y="314.96"/>
<instance part="PATA_3" gate="-1" x="190.5" y="325.12"/>
<instance part="PATA_3" gate="-2" x="190.5" y="322.58"/>
<instance part="PATA_3" gate="-3" x="190.5" y="320.04"/>
<instance part="PATA_3" gate="-4" x="190.5" y="317.5"/>
<instance part="PATA_3" gate="-5" x="190.5" y="314.96"/>
<instance part="PATA_4" gate="-1" x="340.36" y="325.12"/>
<instance part="PATA_4" gate="-2" x="340.36" y="322.58"/>
<instance part="PATA_4" gate="-3" x="340.36" y="320.04"/>
<instance part="PATA_4" gate="-4" x="340.36" y="317.5"/>
<instance part="PATA_4" gate="-5" x="340.36" y="314.96"/>
<instance part="PATA_5" gate="-1" x="375.92" y="325.12"/>
<instance part="PATA_5" gate="-2" x="375.92" y="322.58"/>
<instance part="PATA_5" gate="-3" x="375.92" y="320.04"/>
<instance part="PATA_5" gate="-4" x="375.92" y="317.5"/>
<instance part="PATA_5" gate="-5" x="375.92" y="314.96"/>
<instance part="BAT" gate="1" x="271.78" y="256.54"/>
<instance part="GND1" gate="1" x="274.32" y="256.54"/>
<instance part="BUS" gate="1" x="276.86" y="256.54"/>
<instance part="5V150MA" gate="1" x="279.4" y="256.54"/>
<instance part="D8" gate="1" x="281.94" y="256.54"/>
<instance part="D6" gate="1" x="284.48" y="256.54"/>
<instance part="D5" gate="1" x="287.02" y="256.54"/>
<instance part="D4" gate="1" x="289.56" y="256.54"/>
<instance part="D3" gate="1" x="292.1" y="256.54"/>
<instance part="RX" gate="1" x="294.64" y="256.54"/>
<instance part="TX" gate="1" x="297.18" y="256.54"/>
<instance part="RST" gate="1" x="302.26" y="259.08" rot="R90"/>
<instance part="RESET" gate="1" x="299.72" y="256.54"/>
<instance part="TX2" gate="1" x="302.26" y="261.62" rot="R90"/>
<instance part="RX2" gate="1" x="302.26" y="264.16" rot="R90"/>
<instance part="5V" gate="1" x="302.26" y="266.7" rot="R90"/>
<instance part="GND2" gate="1" x="302.26" y="269.24" rot="R90"/>
<instance part="GND3" gate="1" x="302.26" y="271.78" rot="R90"/>
<instance part="A5" gate="1" x="299.72" y="274.32" rot="R180"/>
<instance part="A4" gate="1" x="297.18" y="274.32" rot="R180"/>
<instance part="A3" gate="1" x="294.64" y="274.32" rot="R180"/>
<instance part="A2" gate="1" x="292.1" y="274.32" rot="R180"/>
<instance part="A1" gate="1" x="289.56" y="274.32" rot="R180"/>
<instance part="A0" gate="1" x="287.02" y="274.32" rot="R180"/>
<instance part="AR" gate="1" x="284.48" y="274.32" rot="R180"/>
<instance part="D13" gate="1" x="281.94" y="274.32" rot="R180"/>
<instance part="D9" gate="1" x="271.78" y="274.32" rot="R180"/>
<instance part="D10" gate="1" x="274.32" y="274.32" rot="R180"/>
<instance part="D11" gate="1" x="276.86" y="274.32" rot="R180"/>
<instance part="D12" gate="1" x="279.4" y="274.32" rot="R180"/>
<instance part="A7" gate="1" x="289.56" y="269.24"/>
<instance part="A6" gate="1" x="287.02" y="269.24"/>
<instance part="VIN" gate="1" x="314.96" y="269.24" rot="R180"/>
<instance part="GND" gate="1" x="317.5" y="269.24" rot="R180"/>
<instance part="SDA" gate="1" x="320.04" y="269.24" rot="R180"/>
<instance part="SDL" gate="1" x="322.58" y="269.24" rot="R180"/>
<instance part="REST" gate="1" x="325.12" y="269.24" rot="R180"/>
<instance part="A_0" gate="1" x="327.66" y="269.24" rot="R180"/>
<instance part="A_1" gate="1" x="330.2" y="269.24" rot="R180"/>
<instance part="A_2" gate="1" x="332.74" y="269.24" rot="R180"/>
<instance part="SD0" gate="1" x="335.28" y="269.24" rot="R180"/>
<instance part="SC0" gate="1" x="337.82" y="269.24" rot="R180"/>
<instance part="SD1" gate="1" x="340.36" y="269.24" rot="R180"/>
<instance part="SC1" gate="1" x="342.9" y="269.24" rot="R180"/>
<instance part="SC7" gate="1" x="314.96" y="254"/>
<instance part="SD7" gate="1" x="317.5" y="254"/>
<instance part="SC6" gate="1" x="320.04" y="254"/>
<instance part="SD6" gate="1" x="322.58" y="254"/>
<instance part="SC5" gate="1" x="325.12" y="254"/>
<instance part="SD5" gate="1" x="327.66" y="254"/>
<instance part="SC4" gate="1" x="330.2" y="254"/>
<instance part="SD4" gate="1" x="332.74" y="254"/>
<instance part="SC3" gate="1" x="335.28" y="254"/>
<instance part="SD3" gate="1" x="337.82" y="254"/>
<instance part="SC2" gate="1" x="340.36" y="254"/>
<instance part="SD2" gate="1" x="342.9" y="254"/>
<instance part="BATERIA" gate="-1" x="238.76" y="271.78"/>
<instance part="BATERIA" gate="-2" x="238.76" y="269.24"/>
<instance part="COMUNICACION" gate="-1" x="238.76" y="261.62"/>
<instance part="COMUNICACION" gate="-2" x="238.76" y="259.08"/>
<instance part="ESC_0" gate="-1" x="231.14" y="340.36"/>
<instance part="ESC_0" gate="-2" x="231.14" y="337.82"/>
<instance part="ESC_0" gate="-3" x="231.14" y="335.28"/>
<instance part="ESC_1" gate="-1" x="254" y="340.36"/>
<instance part="ESC_1" gate="-2" x="254" y="337.82"/>
<instance part="ESC_1" gate="-3" x="254" y="335.28"/>
<instance part="ESC_2" gate="-1" x="276.86" y="340.36"/>
<instance part="ESC_2" gate="-2" x="276.86" y="337.82"/>
<instance part="ESC_2" gate="-3" x="276.86" y="335.28"/>
<instance part="ESC_4" gate="-1" x="325.12" y="340.36"/>
<instance part="ESC_4" gate="-2" x="325.12" y="337.82"/>
<instance part="ESC_4" gate="-3" x="325.12" y="335.28"/>
<instance part="ESC_5" gate="-1" x="350.52" y="340.36"/>
<instance part="ESC_5" gate="-2" x="350.52" y="337.82"/>
<instance part="ESC_5" gate="-3" x="350.52" y="335.28"/>
<instance part="ESC_3" gate="-1" x="299.72" y="340.36"/>
<instance part="ESC_3" gate="-2" x="299.72" y="337.82"/>
<instance part="ESC_3" gate="-3" x="299.72" y="335.28"/>
<instance part="P+1" gate="1" x="180.34" y="330.2"/>
<instance part="P+2" gate="1" x="279.4" y="243.84" rot="R180"/>
<instance part="P+3" gate="1" x="314.96" y="276.86"/>
<instance part="GND4" gate="1" x="228.6" y="350.52" rot="R180"/>
<instance part="GND5" gate="1" x="274.32" y="233.68" rot="MR0"/>
<instance part="GND6" gate="1" x="317.5" y="279.4" rot="MR180"/>
<instance part="GND7" gate="1" x="233.68" y="266.7" rot="MR0"/>
<instance part="GND9" gate="1" x="307.34" y="271.78" rot="MR270"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$6" class="0">
</net>
<net name="D3" class="0">
<segment>
<pinref part="D3" gate="1" pin="MP"/>
<wire x1="292.1" y1="254" x2="292.1" y2="251.46" width="0.1524" layer="91"/>
<label x="292.1" y="251.46" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ESC_0" gate="-3" pin="S"/>
<wire x1="226.06" y1="335.28" x2="228.6" y2="335.28" width="0.1524" layer="91"/>
<label x="226.06" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="PATA_3" gate="-2" pin="S"/>
<wire x1="187.96" y1="322.58" x2="185.42" y2="322.58" width="0.1524" layer="91"/>
<label x="185.42" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A3" gate="1" pin="MP"/>
<wire x1="294.64" y1="279.4" x2="294.64" y2="276.86" width="0.1524" layer="91"/>
<label x="294.64" y="279.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="PATA_2" gate="-2" pin="S"/>
<wire x1="228.6" y1="322.58" x2="223.52" y2="322.58" width="0.1524" layer="91"/>
<label x="223.52" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="1" pin="MP"/>
<wire x1="292.1" y1="287.02" x2="292.1" y2="276.86" width="0.1524" layer="91"/>
<label x="292.1" y="287.02" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="PATA_0" gate="-2" pin="S"/>
<wire x1="264.16" y1="322.58" x2="261.62" y2="322.58" width="0.1524" layer="91"/>
<label x="261.62" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A0" gate="1" pin="MP"/>
<wire x1="287.02" y1="287.02" x2="287.02" y2="276.86" width="0.1524" layer="91"/>
<label x="287.02" y="287.02" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="PATA_1" gate="-2" pin="S"/>
<wire x1="302.26" y1="322.58" x2="297.18" y2="322.58" width="0.1524" layer="91"/>
<label x="297.18" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A1" gate="1" pin="MP"/>
<wire x1="289.56" y1="279.4" x2="289.56" y2="276.86" width="0.1524" layer="91"/>
<label x="289.56" y="279.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SC3" class="0">
<segment>
<pinref part="SC3" gate="1" pin="MP"/>
<wire x1="335.28" y1="241.3" x2="335.28" y2="251.46" width="0.1524" layer="91"/>
<label x="335.28" y="241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_3" gate="-4" pin="S"/>
<wire x1="187.96" y1="317.5" x2="185.42" y2="317.5" width="0.1524" layer="91"/>
<label x="185.42" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD3" class="0">
<segment>
<pinref part="PATA_3" gate="-3" pin="S"/>
<wire x1="187.96" y1="320.04" x2="177.8" y2="320.04" width="0.1524" layer="91"/>
<label x="177.8" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SD3" gate="1" pin="MP"/>
<wire x1="337.82" y1="248.92" x2="337.82" y2="251.46" width="0.1524" layer="91"/>
<label x="337.82" y="248.92" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SD2" class="0">
<segment>
<pinref part="SD2" gate="1" pin="MP"/>
<wire x1="342.9" y1="248.92" x2="342.9" y2="251.46" width="0.1524" layer="91"/>
<label x="342.9" y="248.92" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_2" gate="-3" pin="S"/>
<wire x1="228.6" y1="320.04" x2="215.9" y2="320.04" width="0.1524" layer="91"/>
<label x="215.9" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SC2" class="0">
<segment>
<pinref part="SC2" gate="1" pin="MP"/>
<wire x1="340.36" y1="241.3" x2="340.36" y2="251.46" width="0.1524" layer="91"/>
<label x="340.36" y="241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_2" gate="-4" pin="S"/>
<wire x1="228.6" y1="317.5" x2="223.52" y2="317.5" width="0.1524" layer="91"/>
<label x="223.52" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SC1" class="0">
<segment>
<pinref part="PATA_1" gate="-4" pin="S"/>
<wire x1="302.26" y1="317.5" x2="297.18" y2="317.5" width="0.1524" layer="91"/>
<label x="297.18" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SC1" gate="1" pin="MP"/>
<wire x1="342.9" y1="281.94" x2="342.9" y2="271.78" width="0.1524" layer="91"/>
<label x="342.9" y="281.94" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SD4" class="0">
<segment>
<pinref part="SD4" gate="1" pin="MP"/>
<wire x1="332.74" y1="248.92" x2="332.74" y2="251.46" width="0.1524" layer="91"/>
<label x="332.74" y="248.92" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_4" gate="-3" pin="S"/>
<wire x1="337.82" y1="320.04" x2="327.66" y2="320.04" width="0.1524" layer="91"/>
<label x="327.66" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SC4" class="0">
<segment>
<pinref part="SC4" gate="1" pin="MP"/>
<wire x1="330.2" y1="241.3" x2="330.2" y2="251.46" width="0.1524" layer="91"/>
<label x="330.2" y="241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_4" gate="-4" pin="S"/>
<wire x1="337.82" y1="317.5" x2="335.28" y2="317.5" width="0.1524" layer="91"/>
<label x="335.28" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="SDL" gate="1" pin="MP"/>
<wire x1="322.58" y1="274.32" x2="322.58" y2="271.78" width="0.1524" layer="91"/>
<label x="322.58" y="274.32" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="A5" gate="1" pin="MP"/>
<wire x1="299.72" y1="276.86" x2="299.72" y2="279.4" width="0.1524" layer="91"/>
<label x="299.72" y="279.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="MP"/>
<wire x1="274.32" y1="236.22" x2="274.32" y2="254" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="ESC_1" gate="-1" pin="S"/>
<wire x1="251.46" y1="340.36" x2="251.46" y2="345.44" width="0.1524" layer="91"/>
<junction x="251.46" y="345.44"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="228.6" y1="345.44" x2="228.6" y2="347.98" width="0.1524" layer="91"/>
<junction x="228.6" y="345.44"/>
<wire x1="228.6" y1="345.44" x2="251.46" y2="345.44" width="0.1524" layer="91"/>
<pinref part="ESC_0" gate="-1" pin="S"/>
<wire x1="228.6" y1="340.36" x2="228.6" y2="345.44" width="0.1524" layer="91"/>
<wire x1="251.46" y1="345.44" x2="274.32" y2="345.44" width="0.1524" layer="91"/>
<pinref part="ESC_5" gate="-1" pin="S"/>
<wire x1="274.32" y1="345.44" x2="297.18" y2="345.44" width="0.1524" layer="91"/>
<wire x1="297.18" y1="345.44" x2="322.58" y2="345.44" width="0.1524" layer="91"/>
<wire x1="322.58" y1="345.44" x2="347.98" y2="345.44" width="0.1524" layer="91"/>
<wire x1="347.98" y1="345.44" x2="347.98" y2="340.36" width="0.1524" layer="91"/>
<pinref part="ESC_4" gate="-1" pin="S"/>
<wire x1="322.58" y1="340.36" x2="322.58" y2="345.44" width="0.1524" layer="91"/>
<junction x="322.58" y="345.44"/>
<pinref part="ESC_3" gate="-1" pin="S"/>
<wire x1="297.18" y1="340.36" x2="297.18" y2="345.44" width="0.1524" layer="91"/>
<junction x="297.18" y="345.44"/>
<pinref part="ESC_2" gate="-1" pin="S"/>
<wire x1="274.32" y1="340.36" x2="274.32" y2="345.44" width="0.1524" layer="91"/>
<junction x="274.32" y="345.44"/>
</segment>
<segment>
<pinref part="GND" gate="1" pin="MP"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="317.5" y1="271.78" x2="317.5" y2="276.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="MP"/>
<pinref part="GND2" gate="1" pin="MP"/>
<wire x1="304.8" y1="269.24" x2="304.8" y2="271.78" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<junction x="304.8" y="271.78"/>
</segment>
<segment>
<pinref part="BATERIA" gate="-2" pin="S"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="236.22" y1="269.24" x2="233.68" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PATA_3" gate="-5" pin="S"/>
<wire x1="187.96" y1="314.96" x2="177.8" y2="314.96" width="0.1524" layer="91"/>
<label x="177.8" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_2" gate="-5" pin="S"/>
<wire x1="228.6" y1="314.96" x2="215.9" y2="314.96" width="0.1524" layer="91"/>
<label x="215.9" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_0" gate="-5" pin="S"/>
<wire x1="264.16" y1="314.96" x2="254" y2="314.96" width="0.1524" layer="91"/>
<label x="254" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_1" gate="-5" pin="S"/>
<wire x1="302.26" y1="314.96" x2="289.56" y2="314.96" width="0.1524" layer="91"/>
<label x="289.56" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_4" gate="-5" pin="S"/>
<wire x1="337.82" y1="314.96" x2="327.66" y2="314.96" width="0.1524" layer="91"/>
<label x="327.66" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_5" gate="-5" pin="S"/>
<wire x1="363.22" y1="314.96" x2="373.38" y2="314.96" width="0.1524" layer="91"/>
<label x="363.22" y="314.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="BATERIA+" class="0">
<segment>
<pinref part="BATERIA" gate="-1" pin="S"/>
<wire x1="231.14" y1="271.78" x2="236.22" y2="271.78" width="0.1524" layer="91"/>
<label x="231.14" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="BAT" gate="1" pin="MP"/>
<wire x1="271.78" y1="254" x2="271.78" y2="251.46" width="0.1524" layer="91"/>
<label x="271.78" y="251.46" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="COMUNICACION" gate="-2" pin="S"/>
<wire x1="226.06" y1="259.08" x2="236.22" y2="259.08" width="0.1524" layer="91"/>
<label x="226.06" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RX" gate="1" pin="MP"/>
<wire x1="294.64" y1="254" x2="294.64" y2="241.3" width="0.1524" layer="91"/>
<label x="294.64" y="241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="V+" class="0">
<segment>
<pinref part="5V150MA" gate="1" pin="MP"/>
<pinref part="P+2" gate="1" pin="V+"/>
<wire x1="279.4" y1="254" x2="279.4" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VIN" gate="1" pin="MP"/>
<pinref part="P+3" gate="1" pin="V+"/>
<wire x1="314.96" y1="271.78" x2="314.96" y2="274.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="PATA_5" gate="-1" pin="S"/>
<pinref part="PATA_4" gate="-1" pin="S"/>
<wire x1="337.82" y1="325.12" x2="373.38" y2="325.12" width="0.1524" layer="91"/>
<junction x="337.82" y="325.12"/>
<pinref part="PATA_1" gate="-1" pin="S"/>
<wire x1="302.26" y1="325.12" x2="337.82" y2="325.12" width="0.1524" layer="91"/>
<junction x="302.26" y="325.12"/>
<pinref part="PATA_0" gate="-1" pin="S"/>
<wire x1="264.16" y1="325.12" x2="302.26" y2="325.12" width="0.1524" layer="91"/>
<junction x="264.16" y="325.12"/>
<pinref part="PATA_2" gate="-1" pin="S"/>
<wire x1="228.6" y1="325.12" x2="264.16" y2="325.12" width="0.1524" layer="91"/>
<junction x="228.6" y="325.12"/>
<pinref part="PATA_3" gate="-1" pin="S"/>
<wire x1="187.96" y1="325.12" x2="228.6" y2="325.12" width="0.1524" layer="91"/>
<junction x="187.96" y="325.12"/>
<wire x1="187.96" y1="325.12" x2="180.34" y2="325.12" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="V+"/>
<wire x1="180.34" y1="325.12" x2="180.34" y2="327.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="COMUNICACION" gate="-1" pin="S"/>
<wire x1="236.22" y1="261.62" x2="233.68" y2="261.62" width="0.1524" layer="91"/>
<label x="233.68" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="TX" gate="1" pin="MP"/>
<wire x1="297.18" y1="254" x2="297.18" y2="248.92" width="0.1524" layer="91"/>
<label x="297.18" y="248.92" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="A4" gate="1" pin="MP"/>
<wire x1="297.18" y1="284.48" x2="297.18" y2="276.86" width="0.1524" layer="91"/>
<label x="297.18" y="284.48" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="SDA" gate="1" pin="MP"/>
<wire x1="320.04" y1="271.78" x2="320.04" y2="281.94" width="0.1524" layer="91"/>
<label x="320.04" y="281.94" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<pinref part="D9" gate="1" pin="MP"/>
<wire x1="271.78" y1="276.86" x2="271.78" y2="279.4" width="0.1524" layer="91"/>
<label x="271.78" y="279.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="297.18" y1="335.28" x2="294.64" y2="335.28" width="0.1524" layer="91"/>
<pinref part="ESC_3" gate="-3" pin="S"/>
<label x="294.64" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<pinref part="D10" gate="1" pin="MP"/>
<wire x1="274.32" y1="276.86" x2="274.32" y2="287.02" width="0.1524" layer="91"/>
<label x="274.32" y="287.02" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="ESC_4" gate="-3" pin="S"/>
<wire x1="320.04" y1="335.28" x2="322.58" y2="335.28" width="0.1524" layer="91"/>
<label x="320.04" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="A7" gate="1" pin="MP"/>
<wire x1="289.56" y1="266.7" x2="292.1" y2="266.7" width="0.1524" layer="91"/>
<label x="292.1" y="266.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_5" gate="-2" pin="S"/>
<wire x1="373.38" y1="322.58" x2="370.84" y2="322.58" width="0.1524" layer="91"/>
<label x="370.84" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="A6" gate="1" pin="MP"/>
<wire x1="284.48" y1="266.7" x2="287.02" y2="266.7" width="0.1524" layer="91"/>
<label x="284.48" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_4" gate="-2" pin="S"/>
<wire x1="337.82" y1="322.58" x2="335.28" y2="322.58" width="0.1524" layer="91"/>
<label x="335.28" y="322.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="D6" gate="1" pin="MP"/>
<wire x1="284.48" y1="243.84" x2="284.48" y2="254" width="0.1524" layer="91"/>
<label x="284.48" y="243.84" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="274.32" y1="335.28" x2="271.78" y2="335.28" width="0.1524" layer="91"/>
<pinref part="ESC_2" gate="-3" pin="S"/>
<label x="271.78" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="D5" gate="1" pin="MP"/>
<wire x1="287.02" y1="251.46" x2="287.02" y2="254" width="0.1524" layer="91"/>
<label x="287.02" y="251.46" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<wire x1="251.46" y1="335.28" x2="248.92" y2="335.28" width="0.1524" layer="91"/>
<pinref part="ESC_1" gate="-3" pin="S"/>
<label x="248.92" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D11" class="0">
<segment>
<pinref part="D11" gate="1" pin="MP"/>
<wire x1="276.86" y1="279.4" x2="276.86" y2="276.86" width="0.1524" layer="91"/>
<label x="276.86" y="279.4" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="ESC_5" gate="-3" pin="S"/>
<wire x1="345.44" y1="335.28" x2="347.98" y2="335.28" width="0.1524" layer="91"/>
<label x="345.44" y="335.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD0" class="0">
<segment>
<pinref part="SD0" gate="1" pin="MP"/>
<wire x1="335.28" y1="274.32" x2="335.28" y2="271.78" width="0.1524" layer="91"/>
<label x="335.28" y="274.32" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_0" gate="-3" pin="S"/>
<wire x1="264.16" y1="320.04" x2="254" y2="320.04" width="0.1524" layer="91"/>
<label x="254" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SC0" class="0">
<segment>
<pinref part="SC0" gate="1" pin="MP"/>
<wire x1="337.82" y1="281.94" x2="337.82" y2="271.78" width="0.1524" layer="91"/>
<label x="337.82" y="281.94" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="PATA_0" gate="-4" pin="S"/>
<wire x1="264.16" y1="317.5" x2="261.62" y2="317.5" width="0.1524" layer="91"/>
<label x="261.62" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD1" class="0">
<segment>
<pinref part="PATA_1" gate="-3" pin="S"/>
<wire x1="302.26" y1="320.04" x2="289.56" y2="320.04" width="0.1524" layer="91"/>
<label x="289.56" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SD1" gate="1" pin="MP"/>
<wire x1="340.36" y1="274.32" x2="340.36" y2="271.78" width="0.1524" layer="91"/>
<label x="340.36" y="274.32" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SD5" class="0">
<segment>
<pinref part="PATA_5" gate="-3" pin="S"/>
<wire x1="373.38" y1="320.04" x2="363.22" y2="320.04" width="0.1524" layer="91"/>
<label x="363.22" y="320.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SD5" gate="1" pin="MP"/>
<wire x1="327.66" y1="248.92" x2="327.66" y2="251.46" width="0.1524" layer="91"/>
<label x="327.66" y="248.92" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="SC5" class="0">
<segment>
<pinref part="PATA_5" gate="-4" pin="S"/>
<wire x1="373.38" y1="317.5" x2="370.84" y2="317.5" width="0.1524" layer="91"/>
<label x="370.84" y="317.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SC5" gate="1" pin="MP"/>
<wire x1="325.12" y1="241.3" x2="325.12" y2="251.46" width="0.1524" layer="91"/>
<label x="325.12" y="241.3" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
