#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include <Wire.h>
#include <SPI.h>

#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11 
#define BMP_CS 10

Adafruit_BMP280 bme; // I2C
//Adafruit_BMP280 bme(BMP_CS); // hardware SPI
//Adafruit_BMP280 bme(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

float presion = 0, presion0 = 0;
float temp = 0, temp0 = 0;
float difP, difT;

//TCRT

float amaximo=984.0; // 4 cm
float aminimo=284.0; // 0.0 cm
float distmin=0.0;
float distmax=4.0;

void setup() {
  
  Serial.begin(9600);
  pinMode(A3,INPUT);
  Serial.println(F("BMP280 test"));  
  if (!bme.begin()) {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    while (1);
  }
  presion0 = bme.readPressure()/100;
  temp0 = bme.readTemperature();
  Serial.print("Temperatura Inicial = ");
  Serial.print(temp0);
  Serial.println(" *C");    
  Serial.print("Pressure = ");
  Serial.print(presion0);  //Toledo 1004.2hPa de nivel mar
  Serial.println(" mbares");    
  Serial.println();
}
  
void loop() {

    tcrt();
    leerpresion();
    delay(1000);
}

void leerpresion()
{
    presion = bme.readPressure()/100;
    temp = bme.readTemperature();
    difP = presion0 - presion;
    difT = temp0 - temp;
    
    Serial.print("Temperatura = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");
    
    Serial.print("Presión = ");
    Serial.print(bme.readPressure()/100);  //Toledo 1004.2hPa de nivel mar
    Serial.println(" mbares");

    Serial.print("Dif Temperatura = ");
    Serial.print(difT);
    Serial.println(" *C");

    Serial.print("Dif Presión = ");
    Serial.print(difP);
    Serial.println(" mbares");
    Serial.println();
}

void tcrt()
{
  int lectura;
  float distancia;
  lectura=analogRead(A3);
  distancia=fmap(lectura,aminimo,amaximo,distmin,distmax);
  Serial.print("Distancia: ");
  Serial.print(distancia);
  Serial.println(" cm"); 
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  float valor;
  valor=(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  return valor;
}
