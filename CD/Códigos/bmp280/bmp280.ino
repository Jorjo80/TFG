#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include <Wire.h>
#include <SPI.h>

#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11 
#define BMP_CS 10

Adafruit_BMP280 bme; // I2C
//Adafruit_BMP280 bme(BMP_CS); // hardware SPI
//Adafruit_BMP280 bme(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

float presion = 0, presion0 = 0;
float temp = 0, temp0 = 0;
float difP, difT;

void setup() {
  Serial.begin(9600);
  Serial.println(F("BMP280 test"));
  
  if (!bme.begin()) {  
    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
    while (1);
  }
  presion0 = bme.readPressure()/100;
  temp0 = bme.readTemperature();
  Serial.print("Temperatura Inicial = ");
  Serial.print(temp0);
  Serial.println(" *C");    
  Serial.print("Pressure = ");
  Serial.print(presion0);  //Toledo 1004.2hPa de nivel mar
  Serial.println(" mbares");    
  Serial.println();
}
  
void loop() {

    presion = bme.readPressure()/100;
    temp = bme.readTemperature();
    difP = presion0 - presion;
    difT = temp0 - temp;
    
    Serial.print("Temperatura = ");
    Serial.print(bme.readTemperature());
    Serial.println(" *C");
    
    Serial.print("Presión = ");
    Serial.print(bme.readPressure()/100);  //Toledo 1004.2hPa de nivel mar
    Serial.println(" mbares");

    Serial.print("Dif Temperatura = ");
    Serial.print(difT);
    Serial.println(" *C");

    Serial.print("Dif Presión");
    Serial.print(difP);
    Serial.println(" mbares");
    Serial.println();
    delay(1000);
}

