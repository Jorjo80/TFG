#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include "Wire.h"
#define Sensores 2
Adafruit_BMP280 bmp[Sensores];// I2C

//Adafruit_BMP280 bme(BMP_CS); // hardware SPI
//Adafruit_BMP280 bme(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);

float presion[Sensores], presion_ini[Sensores];
float temp[Sensores], temp_ini[Sensores];
float difP[Sensores];
extern "C" { 
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}
 
#define TCAADDR 0x70
 

// standard Arduino setup()
void setup()
{
    Wire.begin();
    Serial.begin(9600);
    inicializarSensores();
    lecturasIniciales();
}
 
void loop() 
{
  leerbmp();
  diferencias();
  delay(2000);
}

void tcaselect(uint8_t i)
{
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}

void inicializarSensores()
{
  for(int i=0; i < Sensores; i++)
  {
    if(!bmp[i].begin())
    {
      Serial.print("No se encuentra Bmp280 número ");
      Serial.print(i);
      Serial.println(". Comprobar conexión");
      while (1);
    }
    else
    {
      Serial.print("BMP ");
      Serial.print(i);
      Serial.println(" encendido");
    }
  }
}

void leerbmp()//Se leerán los diversos bmp280 disponibles
{
  for(int i=0; i < Sensores; i++)
  {
    tcaselect(i);
    presion[i]=bmp[i].readPressure()/100;
    temp[i] = bmp[i].readTemperature();
    Serial.print("Presión Pata ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(presion[i]);
    Serial.println(" mbares");
    Serial.print("Temperatura Pata ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(temp[i]);
    Serial.println("ºC");
    Serial.println();
  }
}
void lecturasIniciales()
{
  for(int i=0; i < Sensores; i++)
  {
    tcaselect(i);
    presion_ini[i]=bmp[i].readPressure()/100;
    temp_ini[i] = bmp[i].readTemperature();
    Serial.print("Presión Pata ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(presion_ini[i]);
    Serial.println(" mbares");
    Serial.print("Temperatura Pata ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(temp_ini[i]);
    Serial.println("ºC");
    Serial.println();
  }
}

void diferencias()
{
  for(int i=0; i < Sensores; i++)
  {
    difP[i] = presion_ini[i] - presion[i];
    Serial.print("Presión Pata ");
    Serial.print(i);
    Serial.print(" ha variado = ");
    Serial.print(difP[i]);
    Serial.println(" mbares");
  }
}

